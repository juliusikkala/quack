# Depends on SDL2
find_package(SDL2 REQUIRED)
set(SDL2_ttf_INCLUDE_DIR ${SDL2_INCLUDE_DIR})

find_library(
    SDL2_ttf_LIBRARY SDL_ttf.h
    NAMES SDL2_ttf
    HINTS
        ENV SDL2DIR
    PATH_SUFFIXES lib lib64 i386-linux-gnu
    CACHE STRING "Where the SDL2_ttf library is"
)

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(
    SDL2_ttf
    REQUIRED_VARS SDL2_ttf_LIBRARY SDL2_ttf_INCLUDE_DIR
)
