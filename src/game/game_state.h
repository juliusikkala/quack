/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_GAME_STATE_H_
#define QUACK_GAME_STATE_H_
    #include <SDL.h>

    class game;
    class game_state
    {
    public:
        game_state(game& parent);
        virtual ~game_state();

        virtual void handle_event(const SDL_Event& e)=0;
        virtual void update(unsigned dt)=0;
        virtual void render()=0;

        game& get_parent();
        const game& get_parent() const;
    protected:
        game* parent;
    };
#endif

