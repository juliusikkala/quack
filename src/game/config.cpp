/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config.h"
#include "build_config.h"
#define XSTR(s) STR(s)
#define STR(s) #s
#define DEFAULT_TITLE_STR \
    ("Quack " XSTR(QUACK_VERSION_MAJOR) "." XSTR(QUACK_VERSION_MINOR))
#define DEFAULT_WIDTH 800
#define DEFAULT_HEIGHT 600
#define DEFAULT_GL_MAJOR 3
#define DEFAULT_GL_MINOR 3
config::config()
: window{
    DEFAULT_TITLE_STR,
    uvec2(DEFAULT_WIDTH, DEFAULT_HEIGHT),
    false,
    true
  },
  gl{DEFAULT_GL_MAJOR, DEFAULT_GL_MINOR},
  assets{
    "data/"
  }
{
}
