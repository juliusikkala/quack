/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "menu_state.h"
#include "game.h"
#include "game_assets.h"
#include "error.h"
#include <SDL.h>
#include <cmath>

menu_state::menu_state(game& parent)
: game_state(parent)
{
    load();
}
menu_state::~menu_state()
{
    unload();
}
void menu_state::handle_event(const SDL_Event& e)
{
    uvec2 window_size=get_parent().get_config().window.size;
    switch(e.type)
    {
    case SDL_KEYDOWN:
        if(e.key.keysym.sym==SDLK_ESCAPE)
        {
            parent->quit();
        }
        break;
    case SDL_MOUSEMOTION:
        cursor.translate(vec2(e.motion.xrel, e.motion.yrel));
        cursor.set_position(
            clamp(
                cursor.get_position(),
                vec3(0),
                vec3(window_size, 0)
            )
        );
        break;
    default:
        break;
    };
}
void menu_state::update(unsigned dt)
{
    game_assets& assets=get_parent().get_assets();
    uvec2 window_size=get_parent().get_config().window.size;
    double t=SDL_GetTicks()/1000.0;
    background.set_src_rect(
        vec4(
            sin(t)*window_size[0],
            cos(t)*window_size[1],
            window_size
        )
    );
    target.set_rotation(t*10);
    target.set_position(vec2(window_size[0]/2+cos(t)*200, window_size[1]/2+sin(t)*200));
    if(target.is_over(
        cursor.get_position(),
        window_size,
        assets.pixel_projection)
    ){
        cursor.set_texture(assets.cursor_coral);
    }
    else
    {
        cursor.set_texture(assets.cursor_mint);
    }
}
void menu_state::render()
{
    game_assets& assets=get_parent().get_assets();
    background.draw(assets.pixel_projection);
    title.draw(assets.pixel_projection);
    target.draw(assets.pixel_projection);
    cursor.draw(assets.pixel_projection);
    check_gl_error();
}
void menu_state::load()
{
    game_assets& assets=get_parent().get_assets();
    uvec2 window_size=get_parent().get_config().window.size;
    background.set_texture(assets.menu_background);
    background.set_src_rect(vec4(0, 0, window_size));
    cursor.set_texture(assets.cursor_mint);
    cursor.set_scale(2);
    target.set_texture(assets.target);
    target.set_origin(0);
    target.set_position(window_size/2);
    target.set_rotation(M_PI/2);
    text_block title_text;
    title_text.f=&(assets.default_font);
    title_text.text="Quack";
    title_text.ptsize=window_size[1]/10;
    title_text.color=vec3(1,1,1);
    title_text.style=font::NORMAL;
    title_text.outline_width=5;
    title_text.outline_color=vec3(0,0,0);
    title.set_content(title_text);
    title.set_position(vec2(window_size[0]/2, title.get_size()[1]/2));
    title.set_origin(0);
}
void menu_state::unload()
{
}
