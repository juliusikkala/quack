/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_GAME_H_
#define QUACK_GAME_H_
    #include <SDL.h>
    #include "config.h"
    #include "framework/context.h"

    class game_assets;
    class game_state;
    class menu_state;
    class game
    {
    friend class game_state;
    public:
        game(const config& cfg);
        ~game();

        const config& get_config() const;

        game_assets& get_assets();
        const game_assets& get_assets() const;
        context& get_context();
        const context& get_context() const;

        bool run();
        void quit();

        void set_state(game_state* new_state);
    protected:
    private:
        config cfg;
        context ctx;

        game_assets* assets;

        game_state* current_state;
        menu_state* menu;
    };
#endif
