/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_MENU_STATE_H_
#define QUACK_MENU_STATE_H_
    #include "game_state.h"
    #include "sprite.h"
    #include "texture.h"
    #include "label.h"
    #include <SDL.h>
    #include <string>

    class menu_state: public game_state
    {
    public:
        menu_state(game& parent);
        ~menu_state();

        void handle_event(const SDL_Event& e);
        void update(unsigned dt);
        void render();
    protected:
    private:
        void load();
        void unload();
        sprite background;
        sprite cursor, target;
        label title, start, quit;
    };
#endif
