/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "game_assets.h"
#include "game_state.h"
#include "menu_state.h"
#include "framework/error.h"

game::game(const config& cfg)
: cfg(cfg),
  ctx(
    cfg.window.title.c_str(),
    cfg.window.size,
    cfg.window.fullscreen,
    cfg.window.vsync
  ), assets(nullptr), current_state(nullptr), menu(nullptr)
{
    assets=new game_assets(*this);
    menu=new menu_state(*this);
    current_state=menu;
}
game::~game()
{
    current_state=nullptr;
    delete menu;
    delete assets;
}
const config& game::get_config() const
{
    return cfg;
}
game_assets& game::get_assets()
{
    return *assets;
}
const game_assets& game::get_assets() const
{
    return *assets;
}
context& game::get_context()
{
    return ctx;
}
const context& game::get_context() const
{
    return ctx;
}
bool game::run()
{
    unsigned dt=0;
    unsigned begin=SDL_GetTicks(), end=0;
    while(current_state!=nullptr)
    {
        //Cache current_state; it may be changed by handle_event or update.
        game_state* state=current_state;
        //Handle events
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            state->handle_event(event);
            if(event.type==SDL_QUIT)
            {
                current_state=nullptr;
            }
        }
        //Update
        state->update(dt);
        //Render
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
        state->render();
        SDL_GL_SwapWindow(ctx.get_window());
        //Calculate frame time
        end=SDL_GetTicks();
        dt=end-begin;
        begin=end;
    }
    return true;
}
void game::quit()
{
    current_state=nullptr;
}
void game::set_state(game_state* state)
{
    current_state=state;
}
