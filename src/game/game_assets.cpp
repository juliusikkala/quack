/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game_assets.h"
#include "game.h"
#include "framework/error.h"
#include "framework/vec.h"
#include <cstdio>
#include <cstdint>
#include <string>

game_assets::game_assets(game& parent)
: menu_background(parent.get_context()),
  alpha_test(parent.get_context()),
  target(parent.get_context()),
  cursor_mint(parent.get_context()),
  cursor_coral(parent.get_context()),
  default_font(parent.get_context())
{
    const config& cfg=parent.get_config();
    //Load menu background
    menu_background.create(
        (cfg.assets.path+"menu_background.png").c_str()
    );
    alpha_test.create(
        (cfg.assets.path+"alpha_test.png").c_str()
    );
    target.create(
        (cfg.assets.path+"target.png").c_str()
    );
    cursor_mint.create(
        (cfg.assets.path+"cursor_mint.png").c_str()
    );
    cursor_mint.set_filter_mode(GL_NEAREST);
    cursor_coral.create(
        (cfg.assets.path+"cursor_coral.png").c_str()
    );
    cursor_coral.set_filter_mode(GL_NEAREST);
    //Load main font
    default_font.create((cfg.assets.path+"DejaVuSans.ttf").c_str());
    //Create per-pixel projection matrix
    config::window_config wcfg=parent.get_config().window;
    pixel_projection=orthographic_projection(
        vec2(0, wcfg.size[0]),
        vec2(wcfg.size[1], 0),
        vec2(-1, 1)
    );
}
game_assets::~game_assets()
{
    menu_background.destroy();
}
