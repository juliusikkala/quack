/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_ERROR_H_
#define QUACK_ERROR_H_
    #include <stdexcept>
    
    class gl_error: public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };
    class load_error: public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };
    class font_error: public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };
    class init_error: public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };
    class context_mismatch_error: public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };
    #define check_gl_error() check_gl_error_verbose(__FILE__, __LINE__)
    void check_gl_error_verbose(const char* file, unsigned line);
#endif
