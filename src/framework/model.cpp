/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "model.h"

model::model()
: m(nullptr)
{}
model::model(mesh& m)
: m(&m)
{
}
model::~model()
{
}

void model::set_mesh(mesh& m)
{
    this->m=&m;
}
void model::set_texture(texture& t, unsigned index)
{
    textures[index]=&t;
}
void model::clear_texture(unsigned index)
{
    textures.erase(index);
}
void model::clear_textures()
{
    textures.clear();
}

void model::draw(shader& s)
{
    if(m==nullptr)
    {
        return;
    }
    for(auto& it: textures)
    {
        it.second->make_active(it.first);
    }
    m->draw(s);
}
