/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_MESH_H_
#define QUACK_MESH_H_
    #include <GL/glew.h>
    #include <vector>
    #include "vec.h"
    #include "resource.h"
    #define QUACK_POS_ATTRIB 0

    class shader;
    class mesh: public resource
    {
    public:
        mesh(context& ctx);
        mesh(mesh&& o);
        ~mesh();

        void destroy();
        void draw(const shader& s);
        bool is_valid() const;
        void create(
            size_t size,//in vertices
            size_t elements_size=0,
            const GLuint* elements=nullptr
        );
        void set_attribute(
            GLuint attribute_index,
            unsigned vertex_elements,
            const float* data
        );
        template<unsigned d>
        void set_attribute(
            GLuint attribute_index,
            const vec<float, d>* data
        ){
            set_attribute(attribute_index, d, (const float*)data);
        }
    protected:
    private:
        struct attribute
        {
            attribute(GLuint vbo=0, GLuint index=0);
            bool operator<(const attribute& o) const;
            GLuint vbo, index;
        };
        std::vector<attribute> attributes;
        size_t vertices;
        GLuint ebo, vao;
        bool use_ebo;
    };
#endif
