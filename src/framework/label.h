/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_LABEL_H_
#define QUACK_LABEL_H_
    #include "entity.h"
    #include "texture.h"
    #include "text_block.h"
    #include <string>

    class label: public entity
    {
    public:
        label();
        label(const text_block& block, unsigned width_limit=0);
        label(label&& o);
        ~label();

        vec2 get_size() const;

        void set_content(const text_block& block);
        void set_content(const text_block& block, unsigned width_limit);
        const text_block& get_content() const;

        void set_width_limit(unsigned width_limit);
        unsigned get_width_limit() const;

        void draw(const mat4& projection=mat4(1));
    protected:
    private:
        void create();
        void destroy();

        text_block block;
        unsigned width_limit;
        texture* rendered_text;
    };
#endif
