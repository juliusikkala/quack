/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_FONT_H_
#define QUACK_FONT_H_
    #include "resource.h"
    #include "texture.h"
    #include "vec.h"
    #include <SDL_ttf.h>
    #include <string>
    #include <map>

    struct text_block;
    class font: public resource
    {
    public:
        font(context& ctx);
        font(font&& o);
        ~font();

        //The file at 'path' must be valid until destroy() is called. The
        //char* itself may safely be freed after this call.
        void create(const char* path);
        void destroy();
        bool is_valid() const;

        //Makes sure that the font is cached with the given settings.
        void preload(
            size_t ptsize,
            unsigned style=NORMAL,
            unsigned outline=0
        ) const;
        void clear();

        static constexpr unsigned NORMAL=TTF_STYLE_NORMAL;
        static constexpr unsigned BOLD=TTF_STYLE_BOLD;
        static constexpr unsigned ITALIC=TTF_STYLE_ITALIC;
        static constexpr unsigned UNDERLINE=TTF_STYLE_UNDERLINE;
        static constexpr unsigned STRIKETHROUGH=TTF_STYLE_STRIKETHROUGH;

        struct font_settings
        {
            size_t ptsize;
            unsigned style;
            unsigned outline;
            //Implemented for std::less, which is used by std::map
            bool operator<(const font_settings& o) const;
        };
        //Creates a new TTF_Font if a matching one isn't found.
        TTF_Font* get_font(const font_settings& settings) const;
        //Helpers for get_font(font_settings)
        TTF_Font* get_font(const text_block& block) const;
        TTF_Font* get_outline_font(const text_block& block) const;
    private:
        bool valid;
        std::string path;
        //Since changing style or outline_width will flush the font cache,
        //the fonts with the correct settings are cached.
        mutable std::map<font_settings, TTF_Font*> cache;
    };
#endif
