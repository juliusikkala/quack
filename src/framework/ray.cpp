/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ray.h"
ray::ray()
: origin(0), dir(0,0,1)
{}
ray::ray(vec3 origin, vec3 dir)
: origin(origin), dir(dir)
{
}

vec3 ray::get_point(float t) const
{
    return origin+dir*t;
}
ray ray::operator*(const mat4& transform) const
{
    return ray(vec4(origin, 1)*transform, vec4(dir, 0)*transform);
}
