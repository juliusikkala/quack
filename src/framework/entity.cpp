/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "entity.h"

entity::entity()
: scale(1), rotation(1), bounding_box(-1, 1)
{
}
entity::entity(const entity& o)
: pos(o.pos), scale(o.scale), origin(o.origin), rotation(o.rotation),
  bounding_box(o.bounding_box)
{}
entity::~entity()
{}
void entity::set_origin(vec3 pos)
{
    origin=pos;
}
vec3 entity::get_origin() const
{
    return origin;
}

void entity::set_position(vec3 pos)
{
    this->pos=pos;
}
vec3 entity::get_position() const
{
    return pos;
}

void entity::translate(vec3 move)
{
    this->pos+=move;
}

void entity::set_rotation(double angle, vec3 axis)
{
    this->rotation=::rotate(angle, axis);
}
void entity::set_rotation(double roll, double pitch, double yaw)
{
    this->rotation=::rotate(roll, pitch, yaw);
}
void entity::rotate(double angle, vec3 axis)
{
    this->rotation=::rotate(angle, axis)*this->rotation;
}
void entity::rotate(double roll, double pitch, double yaw)
{
    this->rotation=::rotate(roll, pitch, yaw)*this->rotation;
}
mat4 entity::get_rotation() const
{
    return mat4(rotation);
}

void entity::set_scale(vec3 scale)
{
    this->scale=scale;
}
vec3 entity::get_scale() const
{
    return scale;
}

mat4 entity::get_transform() const
{
    mat4 origin_translation=::translate(-origin);
    mat4 translation=::translate(pos);
    mat4 scaling=::scale(scale);
    return translation*mat4(rotation)*scaling*origin_translation;
}
mat4 entity::get_inverse_transform() const
{
    mat4 origin_translation=::translate(origin);
    mat4 translation=::translate(-pos);
    mat4 scaling=::scale(vec3(1)/scale);
    return origin_translation*scaling*mat4(transpose(rotation))*translation;
}
box entity::get_local_bounding_box() const
{
    return bounding_box;
}
bool entity::is_over(
    vec2 screen_coord,
    ivec2 screen_size,
    const mat4& projection
) const
{
    //Position of the pointer in screen space
    vec4 pos=vec4(
        2.0*screen_coord/screen_size-1.0,
        1.0, 1.0
    );
    pos[1]=-pos[1];
    //Position of the pointer in world space
    mat4 inv=get_inverse_transform()*inverse(projection);
    pos=inv*pos;
    //Direction of the z-vector beginning from the pointer in world space
    vec3 dir=inv*vec4(0, 0, -1, 0);
    return bounding_box.intersect(ray(pos, dir));
}
