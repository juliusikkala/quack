/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_TEXT_BLOCK_H_
#define QUACK_TEXT_BLOCK_H_
    #include "texture.h"
    #include "font.h"
    #include <string>
    struct text_block
    {
        text_block(
            const char* text="",
            const font* f=nullptr,
            unsigned ptsize=16,
            vec3 color=vec3(1,1,1),
            unsigned style=font::NORMAL,
            unsigned outline_width=0,
            vec3 outline_color=vec3(0,0,0)
        );
        ~text_block();

        texture render(context& ctx, unsigned wrap_length) const;
        static texture render(
            context& ctx,
            const text_block* blocks,
            size_t blocks_sz=1,
            unsigned wrap_length=0
        );

        std::string text;
        const font* f;
        unsigned ptsize, style, outline_width;
        vec3 color, outline_color;
    };
#endif
