/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_MODEL_H_
#define QUACK_MODEL_H_
    #include "texture.h"
    #include "mesh.h"
    #include <map>

    class model
    {
    public:
        model();
        model(mesh& m);
        ~model();

        void set_mesh(mesh& m);
        void set_texture(texture& t, unsigned index=0);
        void clear_texture(unsigned index=0);
        void clear_textures();

        void draw(shader& s);
    protected:
    private:
        mesh* m;
        std::map<unsigned, texture*> textures;
    };
#endif
