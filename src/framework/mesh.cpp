/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "mesh.h"
#include "error.h"
#include "shader.h"
#include <algorithm>
#include <utility>
#define POS_ATTRIB_LOCATION 0
mesh::attribute::attribute(GLuint vbo, GLuint index)
: vbo(vbo), index(index)
{
}
bool mesh::attribute::operator<(const attribute& o) const 
{
    return index<o.index;
}
mesh::mesh(context& ctx)
: resource(ctx), vertices(0), ebo(0), vao(0), use_ebo(false)
{}
mesh::mesh(mesh&& o)
: resource(o), attributes(std::move(o.attributes)), vertices(o.vertices),
  ebo(o.ebo), vao(o.vao), use_ebo(o.use_ebo)
{
    o.attributes.clear();
    o.vertices=0;
    o.ebo=0;
    o.vao=0;
    o.use_ebo=0;
}
mesh::~mesh()
{
    destroy();
}

void mesh::create(
    size_t size,
    size_t elements_size,
    const GLuint* elements
){
    destroy();
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    if(elements_size!=0&&elements!=nullptr)
    {
        //EBO is used, generate it.
        vertices=elements_size;
        glGenBuffers(1, &ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            elements_size*sizeof(GLuint),
            elements,
            GL_STATIC_DRAW
        );
        check_gl_error();
        use_ebo=true;
    }
    else
    {
        vertices=size;
        use_ebo=false;
    }
}
void mesh::destroy()
{
    if(is_valid())
    {
        for(attribute& a: attributes)
        {
            glDeleteBuffers(1, &(a.vbo));
        }
        attributes.clear();
        glDeleteVertexArrays(1, &vao);
        if(use_ebo)
        {
            glDeleteBuffers(1, &ebo);
        }
        ebo=vao=vertices=0;
        use_ebo=false;
    }
}
void mesh::draw(const shader& s)
{
    if(!is_valid())
    {
        return;
    }
    s.use();
    glBindVertexArray(vao);
    if(use_ebo)
    {
        glDrawElements(GL_TRIANGLES, vertices, GL_UNSIGNED_INT, 0);
    }
    else
    {
        glDrawArrays(GL_TRIANGLES, 0, vertices);
    }
}
bool mesh::is_valid() const
{
    return vao!=0;
}
void mesh::set_attribute(
    GLuint attribute_index,
    unsigned vertex_elements, 
    const float* data
){
    //Create the new buffer
    glBindVertexArray(vao);
    attribute a{0, attribute_index};
    glGenBuffers(1, &(a.vbo));
    glBindBuffer(GL_ARRAY_BUFFER, a.vbo);
    glBufferData(
        GL_ARRAY_BUFFER,
        vertices*sizeof(float)*vertex_elements,
        data,
        GL_STATIC_DRAW
    );
    check_gl_error();
    glVertexAttribPointer(
        attribute_index,
        vertex_elements,
        GL_FLOAT,
        GL_FALSE,
        0, 0
    );
    check_gl_error();
    glEnableVertexAttribArray(attribute_index);
    //Put the buffer in the vector
    auto it=std::lower_bound(attributes.begin(), attributes.end(), a);
    //Check if there is another buffer for the same index
    if(it!=attributes.end()&&it->index==a.index)
    {
        //There is, remove the previous vbo
        glDeleteBuffers(1, &(it->vbo));
        it->vbo=a.vbo;
    }
    else
    {
        attributes.insert(it, a);
    }
}
