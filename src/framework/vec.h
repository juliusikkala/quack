/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_VEC_H_
#define QUACK_VEC_H_
    #include <utility>
    #include <type_traits>
    #include <cmath>

    template<typename T, unsigned d>
    struct vec
    {
        T components[d];
        vec()
        {
            for(unsigned i=0;i<d;i++)
            {
                components[i]=0;
            }
        }

        template<typename ...Args>
        vec(const Args&... values)
        {
            init<0>(values...);
        }

        template<typename oT>
        vec(oT components[d])
        {
            for(unsigned i=0;i<d;i++)
            {
                this->components[i]=(T)components[i];
            }
        }
        template<typename oT>
        vec(oT initial)
        {
            for(unsigned i=0;i<d;i++)
            {
                this->components[i]=(T)initial;
            }
        }
        template<typename oT, unsigned od>
        vec(const vec<oT, od>& other)
        {
            unsigned i=0;
            for(;i<od&&i<d;i++)
            {
                components[i]=(T)other.components[i];
            }
            for(;i<d;i++)
            {
                components[i]=0;
            }
        }

        T& operator[](unsigned i)
        {
            return components[i];
        }
        const T& operator[](unsigned i) const
        {
            return components[i];
        }
        const T* get_array() const
        {
            return &components[0];
        }
    private:
        template<unsigned i, typename std::enable_if<i==d, int>::type=0>
        void init(){}
        template<unsigned i, typename ...Args>
        void init(const T& val, const Args&... values)
        {
            components[i]=val;
            init<i+1>(values...);
        }
        template<unsigned i, typename ...Args, typename oT, unsigned od>
        void init(const vec<oT, od>& val, const Args&... values)
        {
            for(unsigned j=0;j<od;++j)
            {
                components[i+j]=val[j];
            }
            init<i+od>(values...);
        }
    };
    template<typename A, typename B>
    struct promote_base
    {
        static const A a;
        static const B b;
        typedef decltype(a+b) type;
    };
    /** \brief Solves the correct type of an operation between two types.*/
    #define promote(a, b) typename promote_base<a, b>::type

    #define vec_vec_operator(opsymbol) \
        template<typename AT, unsigned Ad, typename BT, unsigned Bd>\
        vec<promote(AT,BT), (Ad>Bd?Ad:Bd)>\
        operator opsymbol(const vec<AT, Ad>& a, const vec<BT, Bd>& b)\
        {\
            const unsigned bigger=Ad>Bd?Ad:Bd;\
            \
            vec<promote(AT,BT), bigger> res;\
            vec<promote(AT,BT), bigger> ra(a);\
            vec<promote(AT,BT), bigger> rb(b);\
            \
            for(unsigned i=0;i<bigger;i++)\
            {\
                res[i]=ra[i] opsymbol rb[i];\
            }\
            return res;\
        }\
        template<typename AT, unsigned Ad, typename BT, unsigned Bd>\
        vec<AT, Ad>&\
        operator opsymbol##=(vec<AT, Ad>& a, const vec<BT, Bd>& b)\
        {\
            return a=a opsymbol b;\
        }
    #define vec_scalar_operator(opsymbol) \
        template<typename T, unsigned d, typename sT>\
        vec<promote(T, sT), d>\
        operator opsymbol(const vec<T, d>& v, sT scalar)\
        {\
            vec<promote(T, sT), d> res(v);\
            for(unsigned i=0;i<d;i++)\
            {\
                res[i] opsymbol##= scalar;\
            }\
            return res;\
        }\
        template<typename T, unsigned d, typename sT>\
        vec<T, d>&\
        operator opsymbol##=(vec<T, d>& a, sT scalar)\
        {\
            return a=a opsymbol scalar;\
        }
    #define scalar_vec_operator(opsymbol) \
        template<typename sT, typename T, unsigned d>\
        vec<promote(T, sT), d>\
        operator opsymbol(sT scalar,const vec<T, d>& v)\
        {\
            vec<promote(T,sT), d> res(scalar);\
            for(unsigned i=0;i<d;i++)\
            {\
                res[i] opsymbol##= v[i];\
            }\
            return res;\
        }
    vec_vec_operator(+)
    vec_vec_operator(-)
    vec_vec_operator(*)
    vec_vec_operator(/)
    vec_scalar_operator(+)
    vec_scalar_operator(-)
    vec_scalar_operator(*)
    vec_scalar_operator(/)
    scalar_vec_operator(+)
    scalar_vec_operator(-)
    scalar_vec_operator(*)
    scalar_vec_operator(/)
    #undef vec_vec_operator
    #undef vec_scalar_operator
    #undef scalar_vec_operator
    template<typename AT, unsigned Ad, typename BT, unsigned Bd>
    bool operator==(const vec<AT, Ad>& a, const vec<BT, Bd>& b)
    {
        const unsigned bigger=Ad>Bd?Ad:Bd;
        /* ivec2(1,2)==ivec3(1,2,0), but
         * ivec2(1,2)!=ivec3(1,2,3)*/
        vec<AT, bigger> ra(a);
        vec<BT, bigger> rb(b);
        for(unsigned i=0;i<bigger;++i)
        {
            if(ra[i]!=rb[i])
            {
                return false;
            }
        }
        return true;
    }
    template<typename AT, unsigned Ad, typename BT, unsigned Bd>
    bool operator!=(const vec<AT, Ad>& a, const vec<BT, Bd>& b)
    {
        return !(a==b);
    }
    template<typename T, unsigned d>
    vec<T, d> operator-(const vec<T, d>& v)
    {
        vec<T, d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i]=-v[i];
        }
        return res;
    }
    template<typename returntype=double, typename T, unsigned d>
    returntype length(const vec<T, d>& v)
    {
        returntype sum_of_squares=0;
        for(unsigned i=0;i<d;i++)
        {
            sum_of_squares+=v[i]*v[i];
        }
        return sqrt(sum_of_squares);
    }
    template<
        typename returntype=double,
        typename AT,
        unsigned Ad,
        typename BT,
        unsigned Bd
    >
    returntype distance(const vec<AT, Ad>& a, const vec<BT, Bd>& b)
    {
        return length<returntype>(a-b);
    }
    template<typename returntype=double, typename T, unsigned d>
    vec<returntype, d> normalize(const vec<T, d>& a)
    {
        return a/length<returntype>(a);
    }
    template<typename AT, unsigned Ad, typename BT, unsigned Bd>
    promote(AT, BT) dot(const vec<AT, Ad>& a, const vec<BT, Bd>& b)
    {
        const unsigned bigger=Ad>Bd?Ad:Bd;

        promote(AT, BT) res=0;
        vec<promote(AT, BT), bigger> ra(a);
        vec<promote(AT, BT), bigger> rb(b);

        for(unsigned i=0;i<bigger;i++)
        {
            res+=ra[i]*rb[i];
        }
        return res;
    }
    template<typename AT, typename BT>
    vec<promote(AT,BT), 3>
    cross(const vec<AT, 3>& a, const vec<BT, 3>& b)
    {

        vec<promote(AT,BT), 3> res(
            a[1]*b[2]-a[2]*b[1],
            a[2]*b[0]-a[0]*b[2],
            a[0]*b[1]-a[1]*b[0]
        );
        return res;
    }
    template<typename iT, unsigned id, typename nT, unsigned nd, typename T>
    vec<promote(promote(nT, iT), T), (nd>id?nd:id)>
    refract(const vec<iT, id>& i, const vec<nT, nd>& n, const T& eta)
    {
        promote(nT, iT) dotni=dot(n, i);
        promote(promote(nT, iT), T) k=1-eta*eta*(1-dotni*dotni);
        return (eta*i-(eta*dotni+sqrt(k))*n)*(k>=0.0);
    }
    template<typename iT, unsigned id, typename nT, unsigned nd>
    vec<promote(nT, iT), (nd>id?nd:id)>
    reflect(const vec<iT, id>& i, const vec<nT, nd>& n)
    {
        return i-2*dot(n, i)*n;
    }
    template<typename AT, unsigned d, typename BT>
    vec<AT, d> max(const vec<AT, d>& a, BT b)
    {
        vec<AT, d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i]=a[i]>b?a[i]:b;
        }
        return res;
    }
    template<typename AT, unsigned d, typename BT>
    vec<promote(AT, BT), d> max(const vec<AT, d>& a, const vec<BT, d>& b)
    {
        vec<promote(AT, BT), d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i]=a[i]>b[i]?a[i]:b[i];
        }
        return res;
    }
    template<typename AT, unsigned d, typename BT>
    vec<AT, d> min(const vec<AT, d>& a, BT b)
    {
        vec<AT, d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i]=a[i]<b?a[i]:b;
        }
        return res;
    }
    template<typename AT, unsigned d, typename BT>
    vec<promote(AT, BT), d> min(const vec<AT, d>& a, const vec<BT, d>& b)
    {
        vec<promote(AT, BT), d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i]=a[i]<b[i]?a[i]:b[i];
        }
        return res;
    }
    template<typename T, unsigned d>
    vec<T, d> floor(const vec<T, d>& v)
    {
        vec<T, d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i]=floor(v[i]);
        }
        return res;
    }
    template<typename T, unsigned d>
    vec<T, d> ceil(const vec<T, d>& v)
    {
        vec<T, d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i]=ceil(v[i]);
        }
        return res;
    }
    template<typename T, unsigned d>
    vec<T, d> trunc(const vec<T, d>& v)
    {
        vec<T, d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i]=trunc(v[i]);
        }
        return res;
    }
    template<typename T, unsigned d, typename U>
    vec<promote(T, U), d> clamp(const vec<T, d>& v, U min_val, U max_val)
    {
        return min(max(v, min_val), max_val);
    }
    template<typename T, unsigned d, typename U>
    vec<promote(T, U), d> clamp(
        const vec<T, d>& v,
        const vec<U, d>& min_val,
        const vec<U, d>& max_val
    ){
        return min(max(v, min_val), max_val);
    }
    #undef promote

    typedef vec<float, 2> vec2;
    typedef vec<float, 3> vec3;
    typedef vec<float, 4> vec4;
    typedef vec<double, 2> dvec2;
    typedef vec<double, 3> dvec3;
    typedef vec<double, 4> dvec4;
    typedef vec<int, 2> ivec2;
    typedef vec<int, 3> ivec3;
    typedef vec<int, 4> ivec4;
    typedef vec<unsigned, 2> uvec2;
    typedef vec<unsigned, 3> uvec3;
    typedef vec<unsigned, 4> uvec4;
#endif
