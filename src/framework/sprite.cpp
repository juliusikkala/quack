/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "sprite.h"
#include "texture.h"

sprite::sprite()
: tex(nullptr)
{
    origin=vec2(-1);
}
sprite::sprite(texture& tex)
: tex(&tex), src_rect(0,0,tex.get_size())
{
    origin=vec2(-1);
    vec2 size=tex.get_size();
    bounding_box.origin=-size/2;
    bounding_box.size=size;
}
sprite::~sprite()
{}

void sprite::set_texture(texture& tex)
{
    this->tex=&tex;
    vec2 size=tex.get_size();
    src_rect=vec4(0, 0, size);
    bounding_box.origin=-size/2;
    bounding_box.size=size;
}
vec2 sprite::get_size() const
{
    return scale*vec2(src_rect[2], src_rect[3]);
}
void sprite::set_src_rect(vec4 rect)
{
    src_rect=rect;
    vec2 size=vec2(src_rect[2], src_rect[3]);
    bounding_box.origin=-size/2;
    bounding_box.size=size;
}
vec4 sprite::get_src_rect() const
{
    return src_rect;
}
void sprite::draw(const mat4& projection)
{
    if(tex!=nullptr)
    {
        tex->draw(
            projection*::translate(pos)*mat4(rotation)*
            mat4(::scale(scale*vec2(src_rect[2], src_rect[3])/2))*
            mat4(::translate(-origin)),
            src_rect
        );
    }
}
