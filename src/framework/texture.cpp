/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "texture.h"
#include "error.h"
#include "context.h"
#include <SDL.h>
#include <SDL_image.h>
texture::texture(context& ctx)
: resource(ctx), tex(0), size(0,0), valid(false)
{}
texture::texture(texture&& o)
: resource(o), tex(o.tex), size(o.size), valid(o.valid)
{
    o.tex=0;
    o.size=uvec2(0);
    o.valid=false;
}
texture::~texture()
{
    destroy();
}
texture& texture::operator=(texture&& o)
{
    destroy();
    ctx=&o.get_context();
    tex=o.tex;
    size=o.size;
    valid=o.valid;
    o.tex=0;
    o.size=uvec2(0);
    o.valid=false;
    return *this;
}
void texture::create(const char* path)
{
    SDL_Surface* image=IMG_Load(path);
    if(!image)
    {
        throw load_error(IMG_GetError());
    }
    create(image);
    SDL_FreeSurface(image);
}
void texture::create(SDL_Surface* surface)
{
    destroy();
    //Convert the surface to RGB8 or RGBA8
    GLint format;
    SDL_Surface* optimized=nullptr;
    SDL_PixelFormat* fmt=nullptr;
    SDL_LockSurface(surface);
    if(surface->format->Amask==0)
    {
        //To RGB    
        format=GL_RGB;
        fmt=SDL_AllocFormat(SDL_PIXELFORMAT_RGB888);
        //For some reason allocformat shoves 4 here and ruins everything.
        fmt->BytesPerPixel=3;
        fmt->BitsPerPixel=24;
    }
    else
    {
        //To RGBA
        format=GL_RGBA;
        //For some reason ABGR is RGBA
        fmt=SDL_AllocFormat(SDL_PIXELFORMAT_ABGR8888);
        fmt->BytesPerPixel=4;
        fmt->BitsPerPixel=32;
    }
    SDL_UnlockSurface(surface);
    optimized=SDL_ConvertSurface(surface, fmt, 0);
    SDL_FreeFormat(fmt);

    //Create texture
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    SDL_LockSurface(optimized);
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        format,
        optimized->w,
        optimized->h,
        0,
        format,
        GL_UNSIGNED_BYTE,
        optimized->pixels
    );
    check_gl_error();
    SDL_UnlockSurface(optimized);
    SDL_FreeSurface(optimized);

    size=uvec2(optimized->w, optimized->h);
    valid=true;
    set_wrap_mode(GL_REPEAT);
    set_filter_mode(GL_LINEAR);
}
void texture::destroy()
{
    if(is_valid())
    {
        glDeleteTextures(1, &tex);
        tex=0;
        size=uvec2(0,0);
        valid=false;
    }
}
bool texture::is_valid() const
{
    return valid;
}
uvec2 texture::get_size() const
{
    return size;
}
void texture::make_active(unsigned i) const
{
    if(is_valid())
    {
        glActiveTexture(GL_TEXTURE0+i);
        glBindTexture(GL_TEXTURE_2D, tex);
    }
}
void texture::set_wrap_mode(GLint mode)
{
    if(is_valid())
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mode);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mode);
    }
}
void texture::set_filter_mode(GLint filter)
{
    if(is_valid())
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
    }
}

void texture::draw(
    const mat4& transform,
    vec4 src_rect
){
    if(!is_valid())
    {
        return;
    }
    context_assets& assets=get_context().get_assets();
    assets.blitter["mvp"]=transform;
    assets.blitter["uv_rect"]=vec4(
        vec2(src_rect[0], src_rect[1])/size,
        vec2(src_rect[2], src_rect[3])/size
    );
    make_active(0);
    assets.rect.draw(assets.blitter);
}
