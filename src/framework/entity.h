/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_ENTITY_H_
#define QUACK_ENTITY_H_
    #include "vec.h"
    #include "mat.h"
    #include "box.h"
    class entity
    {
    public:
        entity();
        entity(const entity& o);
        ~entity();

        void set_origin(vec3 pos);
        vec3 get_origin() const;

        void set_position(vec3 pos);
        vec3 get_position() const;

        void translate(vec3 move);

        void set_rotation(double angle, vec3 axis=vec3(0,0,1));
        void set_rotation(double roll, double pitch, double yaw);
        void rotate(double angle, vec3 axis=vec3(0,0,1));
        void rotate(double roll, double pitch, double yaw);
        mat4 get_rotation() const;

        void set_scale(vec3 scale);
        vec3 get_scale() const;

        mat4 get_transform() const;
        mat4 get_inverse_transform() const;

        box get_local_bounding_box() const;
        bool is_over(
            vec2 screen_coord,
            ivec2 screen_size,
            const mat4& projection=mat4(1)
        ) const;
    protected:
        vec3 pos, scale, origin;
        mat3 rotation;
        box bounding_box;
    };
#endif
