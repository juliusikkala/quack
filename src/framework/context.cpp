/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "context.h"
#include "error.h"
#include <GL/glew.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#define QUACK_SDL_FLAGS (SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_EVENTS)
#define QUACK_IMG_FLAGS (IMG_INIT_PNG)

context::context(
    const char* title,
    uvec2 size,
    bool fullscreen,
    bool vsync
): win(nullptr), ctx(nullptr), assets(nullptr)
{
    create(title, size, fullscreen, vsync);
}
context::~context()
{
    destroy();
}
void context::create(
    const char* title,
    uvec2 size,
    bool fullscreen,
    bool vsync
){
    if(SDL_Init(QUACK_SDL_FLAGS))
    {
        destroy();
        throw init_error(SDL_GetError());
    }
    if((IMG_Init(QUACK_IMG_FLAGS)&QUACK_IMG_FLAGS)!=QUACK_IMG_FLAGS)
    {
        destroy();
        throw init_error(IMG_GetError());
    }
    if(TTF_Init())
    {
        destroy();
        throw init_error(TTF_GetError());
    }
    //Create a window with support for the specified GL version
    SDL_GL_SetAttribute(
        SDL_GL_CONTEXT_PROFILE_MASK,
        SDL_GL_CONTEXT_PROFILE_CORE
    );
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, QUACK_GL_MAJOR);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, QUACK_GL_MINOR);
    win=SDL_CreateWindow(
        title,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        size[0], size[1],
        SDL_WINDOW_OPENGL|(fullscreen?SDL_WINDOW_FULLSCREEN:0)
    );
    if(win==nullptr)
    {
        destroy();
        throw init_error(SDL_GetError());
    }
    //Create context
    ctx=SDL_GL_CreateContext(win);
    if(ctx==nullptr)
    {
        destroy();
        throw init_error(SDL_GetError());
    }
    //Try to use late swap tearing if vsync is enabled
    if(!vsync||SDL_GL_SetSwapInterval(-1)!=0)
    {
        SDL_GL_SetSwapInterval(vsync);
    }
    //Load OpenGL functions
    glewExperimental=GL_TRUE;
    GLenum err=glewInit();
    if(err!=GLEW_OK)
    {
        destroy();
        throw init_error((const char*)glewGetErrorString(err));
    }
    //GLEW may cause GL_INVALID_ENUM, swallow it.
    (void)glGetError();
    assets=new context_assets(*this);
    //Set up needed features
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_SetRelativeMouseMode(SDL_TRUE);
}
void context::destroy()
{
    if(assets!=nullptr)
    {
        delete assets;
        assets=nullptr;
    }
    if(ctx!=nullptr)
    {
        SDL_GL_DeleteContext(ctx);
        ctx=nullptr;
    }
    if(win!=nullptr)
    {
        SDL_DestroyWindow(win);
        win=nullptr;
    }
    if(TTF_WasInit())
    {
        TTF_Quit();
    }
    if(SDL_WasInit(0))
    {
        IMG_Quit();
        SDL_Quit();
    }
}
context_assets& context::get_assets()
{
    return *assets;
}
const context_assets& context::get_assets() const
{
    return *assets;
}
SDL_Window* context::get_window()
{
    return win;
}
static const vec2 rect_vertices[]={
    vec2(-1.0, 1.0),
    vec2(1.0, 1.0),
    vec2(1.0, -1.0),
    vec2(-1.0, -1.0)
};
static const vec2 rect_tex_coords[]={
    vec2(0.0, 1.0),
    vec2(1.0, 1.0),
    vec2(1.0, 0.0),
    vec2(0.0, 0.0)
};
static const GLuint rect_elements[]={
    0, 1, 2,
    2, 3, 0
};
context_assets::context_assets(context& parent)
: rect(parent), blitter(parent)
{
    #include "shaders/blitter_fragment_shader.glsl.h"
    #include "shaders/blitter_vertex_shader.glsl.h"
    blitter.compile(blitter_fragment_shader_src, GL_FRAGMENT_SHADER);
    blitter.compile(blitter_vertex_shader_src, GL_VERTEX_SHADER);
    blitter.link();
    blitter["uv_rect"]=vec4(0,0,1,1);

    rect.create(
        sizeof(rect_vertices)/sizeof(vec2),
        sizeof(rect_elements)/sizeof(GLuint),
        rect_elements
    );
    rect.set_attribute(0, rect_vertices);
    rect.set_attribute(1, rect_tex_coords);
}
context_assets::~context_assets()
{
    rect.destroy();
    blitter.destroy();
}
