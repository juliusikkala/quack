#version 330
layout(location=0) in vec3 pos;
layout(location=1) in vec2 texcoord;
out vec2 uv;
uniform mat4 mvp;
uniform vec4 uv_rect;
void main()
{
    gl_Position=mvp*vec4(pos, 1.0);
    uv=uv_rect.xy+uv_rect.zw*texcoord;
}
