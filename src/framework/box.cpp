/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "box.h"

box::box(vec3 origin, vec3 size)
: origin(origin), size(size)
{}

bool box::intersect(const ray& r) const
{
    double dummy_t;
    return intersect(r, dummy_t);
}
bool box::intersect(const ray& r, double& t) const
{
    vec<float, 6> to=vec<float, 6>(
        (origin-r.origin)/r.dir,
        (origin+size-r.origin)/r.dir
    );
    bool found=false;
    for(unsigned i=0;i<6;++i)
    {
        if(to[i]>=0&&(!found||to[i]<t))
        {
            vec3 p=r.get_point(to[i]);
            bool hit=true;
            //Make sure that the point is on the correct plane
            for(unsigned j=0;j<3;++j)
            {
                //Only the plane is checked, skip the dimension the plane is
                //not on. %3 comes from the fact that we packed 2 vec3's in one
                //vector.
                if(j==i%3) continue;
                if(p[j]<origin[j]||p[j]>origin[j]+size[j])
                {
                    hit=false;
                    break;
                }
            }
            if(hit)
            {
                t=to[i];
                found=true;
            }
        }
    }
    return found;
}
bool box::contains(vec3 point) const
{
    return point[0]>=origin[0]&&point[0]<=origin[0]+size[0]&&
           point[1]>=origin[1]&&point[1]<=origin[1]+size[1]&&
           point[2]>=origin[2]&&point[2]<=origin[2]+size[2];
}

