/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "object.h"
#include "shader.h"

object::object()
: m(nullptr)
{
}
object::object(model& m)
: m(&m)
{
}
object::~object()
{}
void object::set_model(model& m)
{
    this->m=&m;
}
void object::draw(shader& s, const mat4& projection)
{
    if(m==nullptr)
    {
        return;
    }
    s["mvp"]=projection*get_transform();
    m->draw(s);
}
