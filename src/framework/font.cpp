/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "font.h"
#include "error.h"
#include "text_block.h"
#include <vector>
#include <string>

bool font::font_settings::operator<(const font_settings& o) const
{
    return ptsize<o.ptsize||
           (ptsize==o.ptsize&&
            (style<o.style||
             (style==o.style&&
              (outline<o.outline))));
}
font::font(context& ctx)
: resource(ctx), valid(false)
{}
font::font(font&& o)
: resource(o), valid(o.valid), path(std::move(o.path)),
  cache(std::move(o.cache))
{
    o.cache.clear();
    o.path.clear();
    o.valid=false;
}
font::~font()
{
    destroy();
}

void font::create(const char* path)
{
    destroy();
    this->path=path;
    valid=true;
}
void font::destroy()
{
    clear();
    valid=false;
}
bool font::is_valid() const
{
    return valid;
}
void font::preload(size_t ptsize, unsigned style, unsigned outline_width) const
{
    if(!is_valid())
    {
        return;
    }
    font_settings settings={ptsize, style, outline_width};
    auto it=cache.find(settings);
    if(it==cache.end())
    {
        TTF_Font* font=TTF_OpenFont(path.c_str(), ptsize);
        if(font==nullptr)
        {
            throw load_error(TTF_GetError());
        }
        TTF_SetFontOutline(font, outline_width);
        TTF_SetFontStyle(font, style);
        cache.insert(std::pair<font_settings, TTF_Font*>(settings, font));
        if(outline_width>0)
        {
            preload(ptsize, style, 0);
        }
    }
}
void font::clear()
{
    for(auto pair: cache)
    {
        TTF_CloseFont(pair.second);
    }
    cache.clear();
}
TTF_Font* font::get_font(const font_settings& settings) const
{
    preload(settings.ptsize, settings.style, settings.outline);
    return cache[settings];
}
TTF_Font* font::get_font(const text_block& block) const
{
    font_settings settings={block.ptsize, block.style, 0};
    return get_font(settings);
}
TTF_Font* font::get_outline_font(const text_block& block) const
{
    if(block.outline_width==0)
    {
        return nullptr;
    }
    font_settings settings={
        block.ptsize,
        block.style,
        block.outline_width
    };
    return get_font(settings);
}
