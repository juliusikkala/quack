/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_VIEW_H_
#define QUACK_VIEW_H_
    #include "entity.h"

    class view: public entity
    {
    public:
        view();
        void set_projection_ortho(
            vec2 x_range,
            vec2 y_range,
            vec2 z_range=vec2(-1, 1)
        );    
        mat4 get_matrix() const;
        mat4 get_inverse_matrix() const;
    private:
    protected:
        mat4 projection;
    };
#endif
