/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_CONTEXT_H_
#define QUACK_CONTEXT_H_
    #include <SDL.h>
    #include <cstdint>
    #include "shader.h"
    #include "mesh.h"
    #include "vec.h"
    #define QUACK_GL_MAJOR 3
    #define QUACK_GL_MINOR 3


    class context_assets;
    /*Contains all resources needed by the framework itself.*/
    class context
    {
    friend class context_assets;
    public:
        /* RAII is necessary for the context, since other objects depend on its
         * lifetime.*/
        context(
            const char* title,
            uvec2 size,
            bool fullscreen=false,
            bool vsync=true
        );
        ~context();
        

        SDL_Window* get_window();
        
        context_assets& get_assets();
        const context_assets& get_assets() const;
    private:
        void create(
            const char* title,
            uvec2 size,
            bool fullscreen,
            bool vsync
        );
        void destroy();
        SDL_Window* win;
        SDL_GLContext ctx;
        context_assets* assets;
    };
    struct context_assets
    {
    public:
        context_assets(context& parent);
        ~context_assets();

        mesh rect;
        shader blitter;
    private:
    };
#endif
