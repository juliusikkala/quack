/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_TEXTURE_H_
#define QUACK_TEXTURE_H_
    #include <GL/glew.h>
    #include <vec.h>
    #include "resource.h"
    #include "mat.h"

    struct SDL_Surface;
    class texture: public resource
    {
    friend class shader;
    public:
        texture(context& ctx);
        texture(texture&& o);
        ~texture();

        texture& operator=(texture&& o);
        
        void create(const char* path);
        void create(SDL_Surface* surface);
        void destroy();
        bool is_valid() const;
        uvec2 get_size() const;
        void make_active(unsigned i) const;

        void set_wrap_mode(GLint mode);
        void set_filter_mode(GLint filter);

        void draw(
            const mat4& transform,
            vec4 src_rect
        );
    protected:
    private:
        GLuint tex;
        uvec2 size;
        bool valid;
    };
#endif
