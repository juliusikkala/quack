/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_OBJECT_H_
#define QUACK_OBJECT_H_
    #include "model.h"
    #include "entity.h"

    class object: public entity
    {
    public:
        object();
        object(model& m);
        ~object();

        void set_model(model& m);
        void draw(shader& s, const mat4& projection=mat4(1));
    protected:
    private:
        model* m;
    };
#endif
