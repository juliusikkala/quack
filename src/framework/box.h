/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_BOX_H_
#define QUACK_BOX_H_
    #include "vec.h"
    #include "ray.h"

    class box
    {
    public:
        box(vec3 origin=vec3(0), vec3 size=vec3(0));

        bool intersect(const ray& r) const;
        bool intersect(const ray& r, double& t) const;
        bool contains(vec3 point) const;

        vec3 origin, size;
    };
#endif
