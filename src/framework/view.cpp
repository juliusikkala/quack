/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "view.h"

view::view()
: projection(1)
{}
void view::set_projection_ortho(
    vec2 x_range,
    vec2 y_range,
    vec2 z_range
){
    projection=orthographic_projection(x_range, y_range, z_range);
}
mat4 view::get_matrix() const
{
    return projection*get_inverse_transform();
}
mat4 view::get_inverse_matrix() const
{
    return get_transform()*inverse(projection);
}
