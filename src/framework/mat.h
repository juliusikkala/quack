/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_MAT_H_
#define QUACK_MAT_H_
    #include <utility>
    #include <type_traits>
    #include <cmath>
    #include <cstdlib>
    #include "vec.h"
    template<typename T, unsigned d>
    class mat
    {
    private:
        template<unsigned i, typename std::enable_if<i==d*d, int>::type=0>
        void init(){}
        template<unsigned i, typename ...Args>
        void init(const T& val, const Args&... values)
        {
            cols[i/d][i%d]=val;
            init<i+1>(values...);
        }
        template<
            unsigned i,
            typename ...Args,
            typename std::enable_if<i%d==0, int>::type=0
        >
        void init(const vec<T, d>& val, const Args&... values)
        {
            cols[i]=val;
            init<i+d>(values...);
        }
    public:
        vec<T, d> cols[d];
        mat(){}
        mat(const T& n)
        {
            for(unsigned i=0;i<d;i++)
            {
                cols[i][i]=n;
            }
        }
        template<typename ...Args>
        mat(const vec<T, d>& val, const Args&... values)
        {
            init<0>(val, values...);
        }
        template<typename ...Args>
        mat(const T& val, const Args&... values)
        {
            init<0>(val, values...);
        }
        template<typename oT, unsigned od>
        mat(const mat<oT, od> o)
        {
            unsigned md=d<od?d:od;
            for(unsigned i=0;i<md;i++)
            {
                cols[i]=o[i];
            }
            for(unsigned i=md;i<d;i++)
            {
                cols[i][i]=1;
            }
        }
        vec<T, d> row(unsigned y) const
        {
            vec<T, d> res;
            for(unsigned x=0;x<d;x++)
            {
                res[x]=cols[x][y];
            }
            return res;
        }
        void set_row(unsigned y, const vec<T, d>& v)
        {
            for(unsigned x=0;x<d;x++)
            {
                cols[x][y]=v[x];
            }
        }
        vec<T, d>& operator[](unsigned i){return cols[i];}
        const vec<T, d>& operator[](unsigned i) const{return cols[i];}
        const T* get_array() const
        {
            return &(cols[0][0]);
        }
    };
    
    #define typedefmat(name, type) \
        typedef mat<type, 2> name##2;\
        typedef mat<type, 3> name##3;\
        typedef mat<type, 4> name##4;
    typedefmat(mat, float)
    typedefmat(dmat, double)
    typedefmat(imat, int)
    typedefmat(umat, unsigned)
    #undef typedefmat

    #define promote(a, b) typename promote_base<a, b>::type
    #define mat_mat_operator(opsymbol)\
        /* all operators except multiplication are implemented like this to\
           comply with glsl*/ \
        template<typename AT, typename BT, unsigned d>\
        mat<promote(AT, BT), d> operator opsymbol(\
            const mat<AT, d>& a, const mat<BT, d>& b\
        ){\
            mat<promote(AT, BT), d> res;\
            for(unsigned x=0;x<d;x++)\
                res.cols[x]=a.cols[x] opsymbol b.cols[x];\
            return res;\
        }
    #define mat_scalar_operator(opsymbol)\
        template<typename AT, typename BT, unsigned d>\
        mat<promote(AT, BT), d> operator opsymbol(\
            const mat<AT, d>& a, const BT& b\
        ){\
            mat<promote(AT, BT), d> res;\
            for(unsigned x=0;x<d;x++)\
                res.cols[x]=a.cols[x] opsymbol b;\
            return res;\
        }
    #define scalar_mat_operator(opsymbol)\
        template<typename AT, typename BT, unsigned d>\
        mat<promote(AT, BT), d> operator opsymbol(\
            const AT& b, const mat<BT, d>& a\
        ){\
            mat<promote(AT, BT), d> res;\
            for(unsigned x=0;x<d;x++)\
                res.cols[x]=b opsymbol a.cols[x];\
            return res;\
        }
    mat_mat_operator(+)
    mat_mat_operator(-)
    mat_mat_operator(/)
    mat_scalar_operator(+)
    mat_scalar_operator(-)
    mat_scalar_operator(*)
    mat_scalar_operator(/)
    scalar_mat_operator(+)
    scalar_mat_operator(-)
    scalar_mat_operator(*)
    scalar_mat_operator(/)
    #undef mat_mat_operator
    #undef mat_scalar_operator
    #undef scalar_mat_operator

	template<typename AT, typename BT, unsigned d>
    mat<promote(AT, BT), d> operator*(const mat<AT, d>& a, const mat<BT, d>& b)
    {
        mat<promote(AT, BT), d> res;
        for(unsigned x=0;x<d;x++)
            for(unsigned y=0;y<d;y++)
                res.cols[x][y]=dot(a.row(y), b.cols[x]);
        return res;
    }
    template<typename AT, typename BT, unsigned d>
    vec<promote(AT, BT), d> operator*(const mat<AT, d>& a, const vec<BT, d>& b)
    {
        vec<promote(AT, BT), d> res;
        for(unsigned y=0;y<d;y++)
            res[y]=dot(a.row(y), b);
        return res;
    }
    template<typename AT, typename BT, unsigned d>
    vec<promote(AT, BT), d> operator*(
        const vec<AT, d>& a, const mat<BT, d>& b
    ){
        vec<promote(AT, BT), d> res;
        for(unsigned x=0;x<d;x++)
            res[x]=dot(a, b[x]);
        return res;
    }
    template<typename T, unsigned d>
    mat<T, d> operator-(const mat<T, d>& a)
    {
        mat<T, d> res;
        for(unsigned x=0;x<d;x++)
            res.cols[x]=-a.cols[x];
        return res;
    }

    template<typename T, unsigned d>
    mat<T, d> transpose(const mat<T, d>& a)
    {
        mat<T, d> res;
        for(unsigned x=0;x<d;x++)
            res.cols[x]=a.row(x);
        return res;
    }
    template<typename T, unsigned d>
    mat<T, d+1> translate(const vec<T, d>& translation)
    {
        mat<T, d+1> trans(1);
        trans[d]=vec<T, d+1>(translation, 1);
        return trans;
    }
    template<typename T, unsigned d>
    vec<T, d> translation(const mat<T, d+1>& trans)
    {
        return trans[d];
    }

    template<typename T>
    dmat3 rotate(double angle, const vec<T, 3> axis)
    {
        double c=cos(angle);
        double one_minus_c=1-c;
        double s=sin(angle);
        dmat3 res;
        for(unsigned x=0;x<3;x++)
        {
            for(unsigned y=0;y<3;y++)
            {
                res[x][y]=axis[x]*axis[y];
            }
        }
        return one_minus_c*res+dmat3(
            c, axis[2]*s, -axis[1]*s,
            -axis[2]*s, c, axis[0]*s,
            axis[1]*s, -axis[0]*s, c
        );
    }
    inline dmat3 rotate_x(double angle)
    {
        double c=cos(angle);
        double s=sin(angle);
        return dmat3(
            1, 0, 0,
            0, c, s,
            0, -s, c
        );
    }
    inline dmat3 rotate_y(double angle)
    {
        double c=cos(angle);
        double s=sin(angle);
        return dmat3(
            c, 0, -s,
            0, 1, 0,
            s, 0, c
        );
    }
    inline dmat3 rotate_z(double angle)
    {
        double c=cos(angle);
        double s=sin(angle);
        return dmat3(
            c, s, 0,
            -s, c, 0,
            0, 0, 1
        );
    }
    inline dmat3 rotate(double x, double y, double z)
    {
        return rotate_z(z)*rotate_y(y)*rotate_x(x);
    }
    inline dmat2 rotate(double angle)
    {
        double c=cos(angle);
        double s=sin(angle);
        return dmat2(c, s, -s, c);
    }
    template<typename T, unsigned d>
    mat<T, d> scale(vec<T, d> s)
    {
        mat<T, d> res;
        for(unsigned i=0;i<d;i++)
        {
            res[i][i]=s[i];
        }
        return res;
    }
    inline mat4 orthographic_projection(
        vec2 x_range,
        vec2 y_range,
        vec2 z_range=vec2(-1,1)
    ){
        float x_diff=x_range[1]-x_range[0];
        float y_diff=y_range[1]-y_range[0];
        float z_diff=z_range[1]-z_range[0];
        mat4 res(1);
        res[0][0]=2.0/x_diff;
        res[1][1]=2.0/y_diff;
        res[2][2]=-2.0/z_diff;
        res[3]=vec4(
            -(x_range[0]+x_range[1])/x_diff,
            -(y_range[0]+y_range[1])/y_diff,
            -(z_range[0]+z_range[1])/z_diff,
            1
        );
        return res;
    }
    template<typename T, unsigned d>
    T determinant(mat<T, d> m);
    //On gcc, some header defines a macro called minor. Thanks gcc!
    template<typename T, unsigned d>
    T determinant_minor(mat<T, d> m, unsigned i, unsigned j)
    {
        mat<T, d-1> res;
        for(unsigned x1=0, x2=0;x1<d-1;x1++, x2++)
        {
            x2+=(x2==i);
            for(unsigned y1=0, y2=0;y1<d-1;y1++, y2++)
            {
                y2+=(y2==j);
                res[x1][y1]=m[x2][y2];
            }
        }
        return determinant(res);
    }
    template<typename T>
    T determinant(mat<T, 2> m)
    {
        return m[0][0]*m[1][1]-m[0][1]*m[1][0];
    }
    template<typename T, unsigned d>
    T determinant(mat<T, d> m)
    {
        T det=0;
        for(unsigned i=0;i<d;i++)
        {
            det+=m[i][0]*determinant_minor(m, i, 0)*(i&1?-1:1);
        }
        return det;
    }
    template<typename T, unsigned d>
    mat<T, d> inverse(mat<T, d> m)
    {
        mat<T, d> tc;//Transposed cofactor matrix
        for(unsigned i=0;i<d;i++)
        {
            for(unsigned j=0;j<d;j++)
            {
                tc[j][i]=((i+j)&1?-1:1)*determinant_minor(m, i, j);
            }
        }
        return tc/determinant(m);
    }
    #undef promote
#endif
