/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "shader.h"
#include "error.h"
static GLuint& current_program()
{
    static GLuint program=0;
    return program;
}
//Returns the previous program
static GLuint use_program(GLuint program)
{
    GLuint old=current_program();
    if(program!=old)
    {
        glUseProgram(program);
        current_program()=program;
    }
    return old;
}
shader::shader(context& ctx)
: resource(ctx), program(0)
{
    create();
}
shader::shader(shader&& o)
: resource(o), program(o.program)
{
    o.program=0;
}
shader::~shader()
{
    destroy();
}
void shader::create()
{
    if(program==0)
    {
        program=glCreateProgram();
        if(program==0)
        {
            check_gl_error();
        }
    }
}
void shader::compile(const char* src, GLenum shader_type)
{
    //Make sure the shader program exists.
    create();
    GLuint shader=glCreateShader(shader_type);
    if(shader==0)
    {
        check_gl_error();
        return;
    }
    glShaderSource(shader, 1, &src, nullptr);
    glCompileShader(shader);
    //Check compilation status
    GLint status=0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if(status==GL_FALSE)
    {
        char* log=nullptr;
        GLint len=0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        log=(char*)malloc(len+1);
        log[len]=0;
        glGetShaderInfoLog(shader, len, nullptr, log);
        glDeleteShader(shader);
        gl_error error(log);
        free(log);
        throw error;
    }
    //Attach & flag the shader for removal
    glAttachShader(program, shader);
    glDeleteShader(shader);
    check_gl_error();
}
void shader::link()
{
    create();
    glLinkProgram(program);
    GLint status=0;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if(status==GL_FALSE)
    {
        char* log=nullptr;
        GLint len=0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
        log=(char*)malloc(len+1);
        log[len]=0;
        glGetProgramInfoLog(program, len, nullptr, log);
        destroy();

        gl_error error(log);
        free(log);
        throw error;
    }
}
void shader::destroy()
{
    if(program!=0)
    {
        glDeleteProgram(program);
        program=0;
    }
}
void shader::use() const
{
    use_program(program);
}
shader::uniform shader::operator[](const char* uniform_name)
{
    uniform u;
    u.location=glGetUniformLocation(program, uniform_name);
    u.program=program;
    return u;
}
bool shader::is_valid() const
{
    if(program==0)
    {
        return false;
    }
    GLint status=0;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    return program==GL_TRUE;
}
GLuint shader::get_program() const
{
    return program;
}
shader::uniform& shader::uniform::operator=(int v)
{
    use_program(program);
    glUniform1i(location, v);
    return *this;
}
shader::uniform& shader::uniform::operator=(ivec2 v)
{
    use_program(program);
    glUniform2i(location, v[0], v[1]);
    return *this;
}
shader::uniform& shader::uniform::operator=(ivec3 v)
{
    use_program(program);
    glUniform3i(location, v[0], v[1], v[2]);
    return *this;
}
shader::uniform& shader::uniform::operator=(ivec4 v)
{
    use_program(program);
    glUniform4i(location, v[0], v[1], v[2], v[3]);
    return *this;
}
shader::uniform& shader::uniform::operator=(float v)
{
    use_program(program);
    glUniform1f(location, v);
    return *this;
}
shader::uniform& shader::uniform::operator=(vec2 v)
{
    use_program(program);
    glUniform2f(location, v[0], v[1]);
    return *this;
}
shader::uniform& shader::uniform::operator=(vec3 v)
{
    use_program(program);
    glUniform3f(location, v[0], v[1], v[2]);
    return *this;
}
shader::uniform& shader::uniform::operator=(vec4 v)
{
    use_program(program);
    glUniform4f(location, v[0], v[1], v[2], v[3]);
    return *this;
}
shader::uniform& shader::uniform::operator=(double v)
{
    use_program(program);
    glUniform1f(location, v);
    return *this;
}
shader::uniform& shader::uniform::operator=(dvec2 v)
{
    use_program(program);
    glUniform2f(location, v[0], v[1]);
    return *this;
}
shader::uniform& shader::uniform::operator=(dvec3 v)
{
    use_program(program);
    glUniform3f(location, v[0], v[1], v[2]);
    return *this;
}
shader::uniform& shader::uniform::operator=(dvec4 v)
{
    use_program(program);
    glUniform4f(location, v[0], v[1], v[2], v[3]);
    return *this;
}
shader::uniform& shader::uniform::operator=(mat2 m)
{
    use_program(program);
    glUniformMatrix2fv(location, 1, false, m.get_array());
    return *this;
}
shader::uniform& shader::uniform::operator=(mat3 m)
{
    use_program(program);
    glUniformMatrix3fv(location, 1, false, m.get_array());
    return *this;
}
shader::uniform& shader::uniform::operator=(mat4 m)
{
    use_program(program);
    glUniformMatrix4fv(location, 1, false, m.get_array());
    return *this;
}
shader::uniform& shader::uniform::operator=(dmat2 m)
{
    return operator=(mat2(m));
}
shader::uniform& shader::uniform::operator=(dmat3 m)
{
    return operator=(mat3(m));
}
shader::uniform& shader::uniform::operator=(dmat4 m)
{
    return operator=(mat4(m));
}
