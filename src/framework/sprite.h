/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_SPRITE_H_
#define QUACK_SPRITE_H_
    #include "entity.h"
    class texture;
    class sprite: public entity
    {
    public:
        sprite();
        sprite(texture& tex);
        ~sprite();

        void set_texture(texture& tex);

        vec2 get_size() const;

        void set_src_rect(vec4 rect);
        vec4 get_src_rect() const;

        void draw(const mat4& projection=mat4(1));
    protected:
    private:
        texture* tex;
        vec4 src_rect;
    };
#endif
