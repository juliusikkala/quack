/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "label.h"
#include "font.h"
#include <utility>

label::label()
: width_limit(0), rendered_text(nullptr)
{
    origin=vec2(-1);
}
label::label(const text_block& b, unsigned width_limit)
: block(b), width_limit(width_limit), rendered_text(nullptr)
{
    origin=vec2(-1);
    create();
}
label::label(label&& o)
: block(o.block), width_limit(o.width_limit), rendered_text(o.rendered_text)
{
    o.rendered_text=nullptr;
}
label::~label()
{
    destroy();
}

vec2 label::get_size() const
{
    if(rendered_text!=nullptr)
    {
        return scale*rendered_text->get_size();
    }
    else
    {
        return vec2(0,0);
    }
}
void label::set_content(const text_block& block)
{
    destroy();
    this->block=block;
    create();
}
void label::set_content(const text_block& block, unsigned width_limit)
{
    destroy();
    this->block=block;
    this->width_limit=width_limit;
    create();
}
const text_block& label::get_content() const
{
    return block;
}
void label::set_width_limit(unsigned width_limit)
{
    destroy();
    this->width_limit=width_limit;
    create();
}
unsigned label::get_width_limit() const
{
    return width_limit;
}

void label::create()
{
    if(rendered_text==nullptr&&block.f!=nullptr)
    {
        rendered_text=new texture(block.render(
            block.f->get_context(),
            width_limit
        ));
        vec3 size=vec3(rendered_text->get_size(), 0);
        bounding_box.origin=-size/2;
        bounding_box.size=size;
    }
}

void label::destroy()
{
    if(rendered_text!=nullptr)
    {
        delete rendered_text;
        rendered_text=nullptr;
        bounding_box.origin=0;
        bounding_box.size=0;
    }
}
void label::draw(const mat4& projection)
{
    if(rendered_text!=nullptr)
    {
        rendered_text->draw(
            projection*::translate(pos)*mat4(rotation)*
            mat4(::scale(scale*rendered_text->get_size()/2))*
            mat4(::translate(-origin)),
            vec4(0,0,rendered_text->get_size())
        );
    }
}
