/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "error.h"
#include <GL/glu.h>

void check_gl_error_verbose(const char* file, unsigned line)
{
    GLenum err=glGetError();
    if(err!=GL_NO_ERROR)
    {
        throw gl_error(
            std::string(file)+" (line "+std::to_string(line)+"): "+
            (const char*)gluErrorString(err)
        );
    }
}
