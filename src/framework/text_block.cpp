/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "text_block.h"
#include "error.h"
#include <SDL_ttf.h>
#include <vector>

text_block::text_block(
    const char* text,
    const font* f,
    unsigned ptsize,
    vec3 color,
    unsigned style,
    unsigned outline_width,
    vec3 outline_color
): text(text==nullptr?"":text), f(f), ptsize(ptsize), style(style),
   outline_width(outline_width), color(color), outline_color(outline_color)
{}
text_block::~text_block()
{}

texture text_block::render(context& ctx, unsigned wrap_length) const
{
    return render(ctx, this, 1, wrap_length);
}
static SDL_Surface* render_text(TTF_Font* font, const char* text, vec3 color)
{
    SDL_Color col={
        (uint8_t)(color[0]*255),
        (uint8_t)(color[1]*255),
        (uint8_t)(color[2]*255),
        255
    };
    SDL_Surface* rendered_text=TTF_RenderUTF8_Blended(font, text, col);
    if(rendered_text==nullptr)
    {
        throw font_error(TTF_GetError());
    }
    return rendered_text;
}
static size_t utf8_sequence_length(const char* utf8)
{
    unsigned len=0;
    while(*utf8&0x80)
    {
        utf8++;
        len++;
    }
    return (len==0?*utf8!=0:len);
}
struct line
{
    line(): width(0), height(0) {}
    line(line&& o)
    : blocks(std::move(o.blocks)), width(o.width), height(o.height) {}
    std::vector<text_block> blocks;
    unsigned width, height;
};
struct line_data
{
    std::string str;
    unsigned width, height;
};
static constexpr unsigned PER_WORD=0;
static constexpr unsigned PER_CHAR=1;
static size_t fitting(
    TTF_Font* font,
    const char* str,
    line_data& line,
    unsigned max_width,
    unsigned outline_width,
    unsigned per_unit
){
    size_t length=0;
    size_t new_length=0;
    while(*(str+length)!=0)
    {
        switch(per_unit)
        {
        case PER_WORD:
            {
                const char* boundary=strpbrk(str+length, " ,;:.?!-\n");
                if(boundary==nullptr)
                {
                    new_length=strlen(str);
                }
                else
                {
                    new_length=(size_t)(boundary-str)+1;
                }
            }
            break;
        default:
        case PER_CHAR:
            new_length=length+utf8_sequence_length(str+length);
            break;
        };
        new_length+=(new_length==length?1:0);
        line.str.assign(str, new_length);
        unsigned width=0, height=0;
        TTF_SizeUTF8(
            font, line.str.c_str(), (int*)&width, (int*)&height
        );
        width+=outline_width*2;
        height+=outline_width*2;
        if(*(str+new_length-1)=='\n')
        {
            line.width=width;
            line.height=height;
            length=new_length-1;
            line.str.resize(length);
            break;
        }
        else if(width<max_width||max_width==0)
        {
            line.width=width;
            line.height=height;
            length=new_length;
        }
        else
        {
            line.str.resize(length);
            break;
        }
    }
    return length;
}
static std::vector<line_data> calculate_wrap(
    TTF_Font* font,
    const char* str,
    unsigned origin,
    unsigned max_width,
    unsigned outline_width
){
    std::vector<line_data> lines;
    while(*str!=0)
    {
        line_data line;
        if(*str=='\n')
        {
            lines.emplace_back();
            str++;
        }
        if(origin!=0)
        {
            unsigned space_left=max_width-origin;
            //Works by short-circuiting and side-effects.
            if(!fitting(font, str, line, space_left, outline_width, PER_WORD)&&
               (fitting(font, str, line, max_width, outline_width, PER_WORD)||
               !fitting(font, str, line, space_left, outline_width, PER_CHAR)))
            {
                if(!fitting(
                    font, str, line, max_width, outline_width, PER_CHAR
                )){
                    //Must have at least one character per row.
                    line.str.assign(str, 1);
                    TTF_SizeUTF8(
                        font, line.str.c_str(),
                        (int*)&line.width, (int*)&line.height
                    );
                    line.width+=outline_width*2;
                    line.height+=outline_width*2;
                }
                //Starts a new line
                lines.emplace_back();
            }
        }
        else
        {
            if(!fitting(font, str, line, max_width, outline_width, PER_WORD))
            {
                if(!fitting(
                    font, str, line, max_width, outline_width, PER_CHAR
                )){
                    //Must have at least one character per row.
                    line.str.assign(str, 1);
                    TTF_SizeUTF8(
                        font, line.str.c_str(),
                        (int*)&line.width, (int*)&line.height
                    );
                    line.width+=outline_width*2;
                    line.height+=outline_width*2;
                }
            }
        }
        str+=line.str.size();
        lines.push_back(line);
        origin=0;
    }
    return lines;
}
static SDL_Surface* render_lines(
    std::vector<line>& lines,
    unsigned width
){
    unsigned height=0;
    //Calculate surface height
    for(unsigned i=0;i<lines.size();i++)
    {
        height+=lines[i].height;
    }
    if(width==0)
    {
        for(unsigned i=0;i<lines.size();i++)
        {
            width=width>lines[i].width?width:lines[i].width;
        }
    }
    SDL_Surface* combined=SDL_CreateRGBSurface(
        0, width, height,
        32,
#if SDL_BYTEORDER==SDL_BIG_ENDIAN
        0xFF000000
        0x00FF0000,
        0x0000FF00,
        0x000000FF
#else
        0x000000FF,
        0x0000FF00,
        0x00FF0000,
        0xFF000000
#endif
    );
    int y=0;
    //Render all the outlines first (to avoid flushing SDL_ttf's glyph cache
    //unnecessarily)
    for(unsigned i=0;i<lines.size();i++)
    {
        int x=0;
        for(unsigned j=0;j<lines[i].blocks.size();j++)
        {
            text_block& block=lines[i].blocks[j];
            //There is no outline, but x should still be advanced.
            if(block.outline_width==0)
            {
                int block_width=0;
                TTF_SizeUTF8(
                    block.f->get_font(block), block.text.c_str(),
                    &block_width, nullptr
                );
                x+=block_width+block.outline_width*2;
                continue;
            }
            TTF_Font* outline_font=block.f->get_outline_font(block);
            SDL_Surface* rendered=render_text(
                outline_font,
                block.text.c_str(),
                block.outline_color
            );
            SDL_Rect dstrect={x, y, 0, 0};
            x+=rendered->w;
            SDL_BlitSurface(
                rendered,
                nullptr,
                combined,
                &dstrect
            );
            SDL_FreeSurface(rendered);
        }
        y+=lines[i].height;
    }
    y=0;
    //Render the text itself
    for(unsigned i=0;i<lines.size();i++)
    {
        int x=0;
        for(unsigned j=0;j<lines[i].blocks.size();j++)
        {
            text_block& block=lines[i].blocks[j];
            TTF_Font* font=block.f->get_font(block);
            SDL_Surface* rendered=render_text(
                font, block.text.c_str(), block.color
            );
            SDL_Rect dstrect={
                x+(int)block.outline_width,
                y+(int)block.outline_width,
                0, 0
            };
            x+=rendered->w+block.outline_width*2;
            SDL_BlitSurface(
                rendered,
                nullptr,
                combined,
                &dstrect
            );
            SDL_FreeSurface(rendered);
        }
        y+=lines[i].height;
    }
    return combined;
}
texture text_block::render(
    context& ctx,
    const text_block* blocks,
    size_t blocks_sz,
    unsigned wrap_length
){
    std::vector<line> lines(1);
    //Split the text_blocks to several lines.
    for(size_t i=0;i<blocks_sz;i++)
    {
        //Missing font
        if(blocks[i].f==nullptr)
        {
            continue;
        }
        std::vector<line_data> line_strings=calculate_wrap(
            blocks[i].f->get_font(blocks[i]),
            blocks[i].text.c_str(), lines.back().width,
            wrap_length, blocks[i].outline_width
        );
        for(size_t j=0;j<line_strings.size();j++)
        {
            if(line_strings[j].str.size()!=0)
            {
                lines.back().width+=line_strings[j].width;
                lines.back().height=
                    lines.back().height<line_strings[j].height?
                        line_strings[j].height:
                        lines.back().height;
                
                lines.back().blocks.push_back(
                    text_block(
                        line_strings[j].str.c_str(),
                        blocks[i].f,
                        blocks[i].ptsize,
                        blocks[i].color,
                        blocks[i].style,
                        blocks[i].outline_width,
                        blocks[i].outline_color
                    )
                );
            }
            //A new line does not have to be added on the last line.
            if(j!=line_strings.size()-1)
            {
                lines.push_back(line());
            }
        }
    }
    SDL_Surface* combined=render_lines(lines, wrap_length);
    lines.clear();
    texture tex(ctx);
    tex.create(combined);
    SDL_FreeSurface(combined);
    return tex;
}
