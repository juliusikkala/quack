/*
Copyright 2016 Julius Ikkala

This file is part of Quack.

Quack is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quack is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quack.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QUACK_SHADER_H_
#define QUACK_SHADER_H_
    #include <GL/glew.h>
    #include "vec.h"
    #include "mat.h"
    #include "resource.h"

    /*A shader class wrapping OpenGL shaders.
     * Conventions:
     * vertex attribute 0: position
     * vertex attribute 1: texture coordinate
     * uniform mat4 mvp: MVP matrix */
    class shader: public resource
    {
    friend class mesh;
    public:
        shader(context& ctx);
        shader(shader&& o);
        ~shader();

        void create();
        void compile(const char* src, GLenum shader_type);
        void link();
        void destroy();

        bool is_valid() const;
        GLuint get_program() const;

        class uniform
        {
        public:
            uniform& operator=(int v);
            uniform& operator=(ivec2 v);
            uniform& operator=(ivec3 v);
            uniform& operator=(ivec4 v);
            uniform& operator=(float v);
            uniform& operator=(vec2 v);
            uniform& operator=(vec3 v);
            uniform& operator=(vec4 v);
            uniform& operator=(double v);
            uniform& operator=(dvec2 v);
            uniform& operator=(dvec3 v);
            uniform& operator=(dvec4 v);

            uniform& operator=(mat2 v);
            uniform& operator=(mat3 v);
            uniform& operator=(mat4 v);
            uniform& operator=(dmat2 v);
            uniform& operator=(dmat3 v);
            uniform& operator=(dmat4 v);
            GLuint program;
            GLint location;
        };
        uniform operator[](const char* uniform);
    private:
        void use() const;
        GLuint program;
    };

#endif
