#version 330

uniform float time;
uniform sampler2D tex;

in vec2 uv;
out vec4 out_color;

void main()
{
    out_color=vec4(texture(tex, uv).rgb, 1.0);
}
