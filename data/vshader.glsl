#version 330
layout(location=0) in vec2 pos;
layout(location=1) in vec3 in_color;
out vec2 uv;
void main()
{
    gl_Position=vec4(pos, 0.0, 1.0);
    uv=(pos+1)/2;
}
